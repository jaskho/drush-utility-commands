<?php

function onstance_drush_command() {


  $items['ons-show-menu'] = array(
    'description' => dt('Displays callback for a given path/route.'),
    'arguments' => array(
      'path' => dt('Path for route callback lookup'),
    ),
  );
  $items['ons-menu-item-view'] = array(
    'description' => 'Show details of the menu item for a given internal path.',
    'arguments' => array(
      'path' => 'The path for which we retrieve the menu item.',
    ),
    'required-arguments' => 1,
    'examples' => array(
      'menu-item-view node/add' => 'Show the page callback, arguments and include file for the menu item that handles the "node/add" page.',
      'menu-item-view --full admin/content' => 'Show the complete menu item for the "admin/content" page.',
      'menu-item-view --fn-view admin/reports/status' => 'Show the source code of the function callback for "admin/reports/status", which is system_status().',
      'nano `drush menu-item-view --fn-view --pipe admin/reports/status`' => 'Edit the file the contains the menu callback function for the path "admin/reports/status".'
    ),
    'options' => array(
      'full' => 'Show the complete menu item.',
      'fn-view' => 'Show the source code of the callback function.',
      'pipe' => 'Output just the filename of the function.',
    ),
    'aliases' => array('miv'),
  );
  $items['ons-mods-info'] = array(
    'description' => 'Get module info using a freetext or regex search on the machine name, path, human name and description. ',
    'arguments' => array(
      'searchtext' => 'Text string to search for',
    ),
    'options' => array(
      'list' => array(
        'description' => 'Output list of matching extensions, without details',
      ),
      'cs' => array(
        'description' => 'Do case-sensitive search (defaults to case-insensitive)',
      ),
      'regex' => array(
        'description' => 'If this option is set, only one argument will be processed.  Specify the regex WITHOUT the slash wrappers (correct: "my.*regex"; incorrect: "/my.*regex/".  Execution flags are not supported.  All searches are case-insensitive by default.  To change this, set the --cs ("case sensitive") flag.',
      ),
    ),
    'examples' => array(
      'drush ons-mods-info tourn cfs' => 'Get detailed info on all extensions that have the strings "tourn" or "cfs" in any of their descriptor fields',
      'drush ons-mods-info --list --regex "^sites\/all\/.*tourn"' => 'Find out if there\'s a module or theme matching "tourn" under the /sites/all directory.',
    ),
  );
  
  $items['ons-pmi'] = array(
    'description' => 'Customized version of pm-info.',
    'arguments' => array(
      'extensions' => 'A list of modules or themes. You can use the * wildcard at the end of extension names to show info for multiple matches. If no argument is provided it will show info for all available extensions.',
    ),
    'drush dependencies' => array('pm'),
  );

  $items['ons-pmi2'] = array (
# see notes in fxn drush_onstance_ons_pmi2
    'description' => 'Get simple list of modules by status, optionally filtering by simple regex.',
    'arguments' => array(
      'searchtext' => 'Text string to search for',
    ),
    'options' => array(
      'az' => array(
        'description' => 'Don\'t group by status',
      ),
    ),
    'examples' => array(
      'drush ons-pmi2 tourn|cfs' => 'Get detailed info on all extensions that have the strings "tourn" or "cfs" in their name',
      'drush ons-pmi2 --az' => 'Don\'t group results by status',
    ),

  ); 
  return $items;  
}

 /**
 * Command handler. Show the menu item for a given internal path.
 */
function drush_devel_menu_item_view($path) {
  $item = menu_get_item($path);

  if ($item !== FALSE) {
    if (drush_get_option('full')) {
      drush_print_r($item);
    }
    else if (drush_get_option('fn-view')) {
      include_once $item['include_file'];
      drush_devel_fn_view($item['page_callback']);
    }
    else {
      $output = array(
        'page_callback' => $item['page_callback'],
        'page_arguments' => unserialize($item['page_arguments']),
        'include_file' => $item['include_file'],
      );
      drush_print_r($output);
    }
  }
  else {
    return drush_set_error(dt('Invalid path'));
  }
}

/**
 * Completion for the menu item view command.
 */
function drush_devel_menu_item_view_complete() {
  $paths = db_query('SELECT path FROM {menu_router}')->fetchCol();
  return array('values' => $paths);
}

function drush_onstance_cms_show_menu($path) {
  echo "here!\n";
  echo $path . "\n";
}

/**
 * Simple list of mods by status
 */
function drush_onstance_ons_pmi2() {
	// This is work in progress, trying to drush-ify the following bash script:
	// d=`drush pmi`
	//
	// echo "$d" | grep -P "(Extension|Status)" | sed 's/[ ]*$//' | sed 'N;s/\n Status/ -- /;s/Extension[^:]*:[ ]*//;s/--[^:]*://;s/^ //;s/^\([^ ]\+\)[ ]\+/\1\t/' | awk '{printf("%-30s %s\n", $1, $2)}' | sort | grep -P "(tourn|reg|ent|cfs)"
	//
	// See also /usr/local/bin/drushpmix.sh
	//
	//
	//
	//




  $args = pm_parse_arguments(func_get_args());

  // $ignore_case = ! drush_get_option('cs');
  // 
  // if(drush_get_option('regex')) {
  //   $regex = $args[0];
  // } else {
  //   $regex = '(' . implode('|', $args) . ')';
  // }

  // $regex = '/' . $regex . '/' . ($ignore_case ? 'i' : '');

  // d=`drush pmi`

	 //  echo "$d" | grep -P "(Extension|Status)" | sed 's/[ ]*$//' | sed 'N;s/\n Status/ -- /;s/Extension[^:]*:[ ]*//;s/--[^:]*://;s/^ //;s/^\([^ ]\+\)[ ]\+/\1\t/' | awk '{printf("%-30s %s\n", $1, $2)}' | sort | grep -P "(tourn|reg|ent|cfs)"





  $extension_info = drush_get_extensions(FALSE);

  // Assemble search arrays
  $exts_by_mach_name = array();
  $exts_by_uri = array();
  $exts_by_long_name = array();
  $exts_by_description = array();
  foreach($extension_info as $mach_name => $extension) {
    $exts_by_mach_name[$mach_name] = $mach_name;
    if(property_exists($extension, 'uri')) {
      $exts_by_uri[$mach_name] = $extension->uri;
    }
    if(property_exists($extension, 'info')) {
      $info = $extension->info;
      if(array_key_exists('name', $info)) {
        $exts_by_long_name[$mach_name] = $info['name'];
      }
      if(array_key_exists('description', $info)) {
        $exts_by_description[$mach_name] = $info['description'];
      }
    }
  }

  // Filter each search array
  $filtered_exts_by_mach_name = preg_grep ( $regex , $exts_by_mach_name);
  $filtered_exts_by_uri = preg_grep ( $regex , $exts_by_uri);
  $filtered_exts_by_long_name = preg_grep ( $regex , $exts_by_long_name);
  $filtered_exts_by_description = preg_grep ( $regex , $exts_by_description);
  
  // combine all arrays
  $filtered_exts = array_merge(
    $filtered_exts_by_mach_name,
    $filtered_exts_by_uri,
    $filtered_exts_by_long_name,
    $filtered_exts_by_description
  );
  $ext_keys = array_keys($filtered_exts);

  if(count($ext_keys) == 0) {
    drush_print("No matching extensions found");
    return;
  }

  if(drush_get_option('list')) {
    drush_print(implode("\n", $ext_keys));
  } else {
    // Pass items to report generator
    drush_ons_pmi_init();
    drush_onstance_ons_pmi($ext_keys);
  }

}
/**
 * Quasi free-text search of modules & themes.
 * Regex support (experimental)
 * 
 * @return [type] [description]
 */
function drush_onstance_ons_mods_info() {
  $args = pm_parse_arguments(func_get_args());

  $ignore_case = ! drush_get_option('cs');
  
  if(drush_get_option('regex')) {
    $regex = $args[0];
  } else {
    $regex = '(' . implode('|', $args) . ')';
  }

  $regex = '/' . $regex . '/' . ($ignore_case ? 'i' : '');

  // $search_text = array_shift($args);

  // // $is_partial = ! preg_match('/^\/.*\/([imsxeADSUXJu]*|)$/', $search_text);
  // $is_partial = ! preg_match('/^regex:/', $search_text);
  // if($is_partial) {
  //   $regex = '/.*' . $search_text . '.*/i';
  // } else {
  //   preg_match('/^(regex:)(([i]*):|)(.*)$/', $search_text, $matches);
  //   $regex = '/' . preg_replace('/^regex:/', '', $search_text) . '/';
  //   $flags = $matches[3];
  //   $test = $matches[4];
  //   $regex = '/' . $test . '/' . $flags;
  // }

  $extension_info = drush_get_extensions(FALSE);

  // Assemble search arrays
  $exts_by_mach_name = array();
  $exts_by_uri = array();
  $exts_by_long_name = array();
  $exts_by_description = array();
  foreach($extension_info as $mach_name => $extension) {
    $exts_by_mach_name[$mach_name] = $mach_name;
    if(property_exists($extension, 'uri')) {
      $exts_by_uri[$mach_name] = $extension->uri;
    }
    if(property_exists($extension, 'info')) {
      $info = $extension->info;
      if(array_key_exists('name', $info)) {
        $exts_by_long_name[$mach_name] = $info['name'];
      }
      if(array_key_exists('description', $info)) {
        $exts_by_description[$mach_name] = $info['description'];
      }
    }
  }

  // Filter each search array
  $filtered_exts_by_mach_name = preg_grep ( $regex , $exts_by_mach_name);
  $filtered_exts_by_uri = preg_grep ( $regex , $exts_by_uri);
  $filtered_exts_by_long_name = preg_grep ( $regex , $exts_by_long_name);
  $filtered_exts_by_description = preg_grep ( $regex , $exts_by_description);
  
  // combine all arrays
  $filtered_exts = array_merge(
    $filtered_exts_by_mach_name,
    $filtered_exts_by_uri,
    $filtered_exts_by_long_name,
    $filtered_exts_by_description
  );
  $ext_keys = array_keys($filtered_exts);

  if(count($ext_keys) == 0) {
    drush_print("No matching extensions found");
    return;
  }

  if(drush_get_option('list')) {
    drush_print(implode("\n", $ext_keys));
  } else {
    // Pass items to report generator
    drush_ons_pmi_init();
    drush_onstance_ons_pmi($ext_keys);
  }

}


function drush_ons_pmi_init() {
  // Load supporting file(s) from pm-info
  drush_command_include('pm-info');
}


/**
 * Provides a reduced version of pm-info output for readability
 * (Namely, skips the keys 'requires', 'required by', 'files')
 *
 * Code from drush/commands/pm/info.pm::drush_pm_info()
 */
function drush_onstance_ons_pmi() {

  $args = func_get_args();

  // Handle indirect call (eg, via ons-mods-info)
  if(is_array($args[0])) {
    $args = $args[0];
  } else {
    $args = pm_parse_arguments($args);
  }

  $extension_info = drush_get_extensions(FALSE);
  _drush_pm_expand_extensions($args, $extension_info);
  // If no extensions are provided, show all.
  if (count($args) == 0) {
    $args = array_keys($extension_info);
  }

  foreach ($args as $extension) {
    if (isset($extension_info[$extension])) {
      $info = $extension_info[$extension];
    }
    else {
      drush_log(dt('!extension was not found.', array('!extension' => $extension)), 'warning');
      continue;
    }
    if ($info->type == 'module') {
      $data = _drush_ons_info_module($info);
    }
    else {
      $data = _drush_pm_info_theme($info);
    }
    drush_print_table(drush_key_value_to_array_table($data));
    print "\n";
  } 
}

/**
 * Factored out to allow for direct call/args passing
 */
function _drush_onstance_ons_pmi() {

  $args = func_get_args();
drush_print_r($args);return;

  $extension_info = drush_get_extensions(FALSE);
  _drush_pm_expand_extensions($args, $extension_info);
  // If no extensions are provided, show all.
  if (count($args) == 0) {
    $args = array_keys($extension_info);
  }

  foreach ($args as $extension) {
    if (isset($extension_info[$extension])) {
      $info = $extension_info[$extension];
    }
    else {
      drush_log(dt('!extension was not found.', array('!extension' => $extension)), 'warning');
      continue;
    }
    if ($info->type == 'module') {
      $data = _drush_ons_info_module($info);
    }
    else {
      $data = _drush_pm_info_theme($info);
    }
    drush_print_table(drush_key_value_to_array_table($data));
    print "\n";
  } 
}


/**
 * Provides a reduced version of pm-info output for readability
 * (Namely, skips the keys 'requires', 'required by', 'files')
 *
 * Code from drush/commands/pm/info.pm::_drush_pm_info_module()
 */
function _drush_ons_info_module($info) {
  $major_version = drush_drupal_major_version();

  $data = _drush_pm_info_extension($info);
  if ($info->schema_version > 0) {
    $schema_version = $info->schema_version;
  }
  elseif ($info->schema_version == -1) {
    $schema_version = "no schema installed";
  }
  else {
    $schema_version = "module has no schema";
  }
  $data['Schema version'] = $schema_version;
  // if ($major_version == 7) {
  //   $data['Files'] = implode(', ', $info->info['files']);
  // }
  // if (count($info->info['dependencies']) > 0) {
  //   $requires = implode(', ', $info->info['dependencies']);
  // }
  // else {
  //   $requires = "none";
  // }
  // $data['Requires'] = $requires;

  // if ($major_version == 6) {
  //   $requiredby = !empty($info->info['dependents'])?$info->info['dependents']:array("none");
  // }
  // else {
  //   $requiredby = !empty($info->required_by)?array_keys($info->required_by):array("none");
  // }
  // $data['Required by'] = implode(', ', $requiredby);

  return $data;
}



/**
 * Command callback. Show detailed info for one or more extensions.
 */
function _onstance_drush_pm_info() {

}
